package net.cursar.android.codigosecreto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void startMission(View v){
        Log.i("MAIN","Going to start activity mission");

        Intent intent=new Intent(this,Mission1Activity.class);

        switch (v.getId()){
            case R.id.mission1:
                intent.putExtra("mission",1);
                break;
            case R.id.mission2:
                intent.putExtra("mission",2);
                break;
            case R.id.mission3:
                intent.putExtra("mission",3);
                break;
            case R.id.mission4:
                intent.putExtra("mission",4);
                break;
            case R.id.mission5:
                intent.putExtra("mission",5);
                break;
            case R.id.mission6:
                intent.putExtra("mission",6);
                break;
            case R.id.mission7:
                intent.putExtra("mission",7);
                break;
        }
        startActivity(intent);
    }
}

