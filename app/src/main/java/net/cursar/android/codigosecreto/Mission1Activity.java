package net.cursar.android.codigosecreto;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;

public class Mission1Activity extends AppCompatActivity {

    private ArchitectView architectView;
    private int mission;

    LocationManager locManager;
    LocationListener locListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_architect);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Intent intent = getIntent();
        mission = intent.getIntExtra("mission", 0);

        if (ArchitectView.isDeviceSupported(this)) {
            this.architectView = (ArchitectView) this.findViewById(R.id.architectView);
            final StartupConfiguration config = new StartupConfiguration("BHEko0BUt5hneouZaQIWco2Hh1X921LTsHcVMKGulqXNc/2CNnGpa6I0a0RQL38Hz6qo8aDAg3xk2DbMMLTfm1bNWI+k41jcMhAAFlBLvN6VHs0HazPvTBWs8fqhdJZITaecuD4vYziuNlxQtlMRsE0+Tj2rgllgd4XdpbJ4cydTYWx0ZWRfX9Y4DKE3V/WNqBfJqtozBIbnnTW8R262PfFuTa/SS5CZ6+7z7gFuiM8FVk8XCBlzHd0mETaT6x1YevE1rSg3jMUaANZHfahh/FUf1xg1fhjdGYe2+HvNZGzF+qcuJEEN5G8gHjByYrBA2u4MzBAl8jRaUFereUJK45zjWgDNqJ5Jdf3ou7aR6syWhD3wHYbT2KWXr7cQQc/W7L4t7BZ3+czHk2euYgUk2FTmQKT1VUNzV53bUj+U3/efMN9ZbhhWkno/xAps+GHcI3AIbPN+JBeI1RwxTRdvv4KP8fcv12343moJtKGfHLjc/4xnZt2mM3ikSF3uWTzX/CMQSNUhutb5N/yh7aTRXqgjVITgESXvP0deBGRxT9z5bauq/+D3T9/hDYLGSyGF26/cM/uZ+MUTW7w9B3nQJcnkuBidONucHusvo9IZjkmn8ZMoBRToaE7sDJpAItGAjq+EEmxHhhjW3jsSamSYQjHelEwpMjzsJziqx5MDXqW2wf5kqj9M7M8BQ87CRYP1"
                    , StartupConfiguration.Features.Geo);
            this.architectView.onCreate(config);

            this.architectView.onPostCreate();
            try {

                this.architectView.load("mission" + mission + "/index.html");
                Log.i("MISSION", "loaded mission" + mission + "/index.html");
            } catch (Exception e) {
                Log.e("MISSION", "Error loading html for mission " + mission);
            }

            locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            locListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    forwardLocation(location);

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

        } else {
            Log.e("MISSION", "Device not supported!");
        }

    }

    private void forwardLocation(Location location){
        Log.i("MISSION", "Location updated to " +
                        location.getLatitude() + " - " + location.getLongitude()
        );
        if (location.hasAltitude()) {
            architectView.setLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    location.getAltitude(),
                    location.getAccuracy()
            );
        } else {
            architectView.setLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    location.getAccuracy()
            );
        }
    }

    @Override
    protected void onDestroy() {
        if (this.architectView != null) {
            this.architectView.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (this.architectView != null) {
            this.architectView.onPause();
        }
        super.onPause();
        try {
            Log.i("MISSION", "Removing updates");
            locManager.removeUpdates(locListener);
        }catch(SecurityException e){
            Log.e("MISSION","Not enough permissions to get location updates");
        }
    }

    @Override
    protected void onResume() {
        if(this.architectView!=null) {
            this.architectView.onResume();
        }
        super.onResume();

        try {

            Log.i("MISSION", "getting last updates");
            Location gpsLast=locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location netwkLast=locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location best=null;
            if(netwkLast==null){
                best=gpsLast;
            }else if(gpsLast==null){
                best=netwkLast;
            }else{
                if(gpsLast.hasAccuracy() && netwkLast.hasAccuracy()){
                    best=gpsLast.getAccuracy()<netwkLast.getAccuracy()?gpsLast:netwkLast;
                }else{
                    best=gpsLast.getTime()>netwkLast.getTime()?gpsLast:netwkLast;
                }
            }
            if(best!=null)
                forwardLocation(best);

            Log.i("MISSION","adding updates");
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 2.0f, locListener);
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 2.0f, locListener);
        }catch(SecurityException e){
            Log.e("MISSION","Not enough permissions to get location updates");
        }
    }



}

