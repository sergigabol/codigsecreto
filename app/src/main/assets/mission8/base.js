var World={

    pois:[
        {   lat:41.416641,
            long:2.133615,
            alt:260,
            desc:"POI 1r"
        },{
            lat:41.417194,
            long:2.132878,
            alt:265,
            desc:"POI segon"
        }
    ],

    init:function(){

        var pois=World.pois;

        for(var i=0; i<pois.length; i++){
            var location=
                new AR.GeoLocation(
                    pois[i].lat,
                    pois[i].long,
                    pois[i].alt
                   );

            var nearLocation =
                new AR.RelativeLocation(
                    location,
                    10,
                    -15,
                    0
                );



            var camDrawable=
                new AR.Label(pois[i].desc,5,
                    {style:{
                            backgroundColor: "#FF0000",
                            textColor: "#00FF00"
                        }
                    }
                );

            var indicatorDrawable=new AR.ImageDrawable(
                new AR.ImageResource("indi.png"),
                1
            );

            var geoObject=new AR.GeoObject(
                location,
                {
                    drawables:{
                        cam: [camDrawable],
                        indicator: indicatorDrawable
                    }
                }
            );

            var relativeGeoObject=new AR.GeoObject(
                 nearLocation,
                 {
                    drawables:{
                        cam: [camDrawable]
                    }
                 }
            );

        }

    }
};

var loaded=false;
AR.context.onLocationChanged = function(lat,long,alt,acc){
    if(!loaded){
        alert("location changed");
        World.init();
        loaded=true;
    }else{
        //update something?

    }
};