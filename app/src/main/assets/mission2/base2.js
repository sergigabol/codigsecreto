var loaded=false;
var WorldBase={
	
    pois:[
    {
		lat:41.365253,
		long:2.088175,
		alt:137.0000, 
		// lat:41.417231,
		// long:2.132933,
		//alt:265,
		desc:"POI 1"
		},{
		lat:41.417006,
		long:2.133204,
		alt:260,
		desc:"POI 2"
		},{
		lat:41.416672,
		long:2.133409,
		alt:255,
		desc:"POI 3"
	}
	
    ],
	
	
    //función de inicio
    init: function(){
		
        var pois= WorldBase.pois;
		
        for (var i=0;i<pois.length;i++) {
            var location=
			new AR.GeoLocation(
			pois[i].lat,
			pois[i].long,
			pois[i].alt
			);
			
            pois[i].loc=location; // guarda la localización
            pois[i].dist = location.distanceToUser(); // guarda la distancia

            Log.i.("Distancia", "distancia del POI "+i+ ""+pois[i].dist);
			
			/*            var nearLocation =
                new AR.RelativeLocation(
				location,10,-15,0
			);*/
		}
	},
	
	creaDrawable: function () {
		
		for (var i=0;i<pois.length;i++) {

			var camDrawable=
			//"estas a "+location.distanceToUser()+" m."
			//Label es un drawable sencillo que dibuja un texto.
			new AR.Label(
			//pois[i].desc,
			"estas a "+pois[i].dist+" m.",
			0.5,
			{
				style:{
					backgroundColor:"#000000",
					textColor:"#FF0000"
				}
			}
			);
			pois[i].label=camDrawable;
			
			var indicatorDrawable= new AR.ImageDrawable(
			new AR.ImageResource("fireball.png"),
			0.1
			);
			
			var bombDrawable=new AR.ImageDrawable(new AR.ImageResource("bomb.png"),1.5);
			
			var geoObject = new AR.GeoObject(
			pois[i].loc,
			{
				drawables: {
					// cam:[bombDrawable,pois[i].label],
					cam:[pois[i].label],
					
					indicator: indicatorDrawable
				}
			}
			
			);
			
			/*                var relativeGeoObject = new AR.GeoObject(
				nearLocation,
				{
				drawables: {
				cam:[camDrawable]
				}
				}
				
			);*/
			}
			
			
			//var location=new AR.GeoLocation(41.417231,2.132933);
			//var distToBomb=location.distanceToUser();
			
			//var bombDrawable=new AR.ImageDrawable("mission2/bomb.png",1);
			
		}
		
		
	};
	
	AR.context.onLocationChanged = function(lat,long,alt,acc){
		//alert("location changed");
		if(loaded==true){
			for(var i=0;i<WorldBase.pois.length;i++){
				
				var local = WorldBase.pois[i].loc;
				WorldBase.pois[i].dist=local.distanceToUser();
				// WorldBase.pois[i].label=distToBomb;
				
			}
			
			WorldBase.creaDrawable();
			//distToBomb= WorldBase.bombsLocations[0].distanceToUser();
			//alert("dist: "+distToBomb);
			}else{
			loaded=true;
			WorldBase.init();
            WorldBase.creaDrawable();
		}
	}
	
