var loaded=false;

var WorldBase={

    pois:[
    {
    lat:41.417231,
    long:2.132933,
    alt:265,
    desc:"POI 1"
    },{
    lat:41.417006,
    long:2.133204,
    alt:260,
    desc:"POI 2"
    },{
    lat:41.416672,
    long:2.133409,
    alt:255,
    desc:"POI 3"
    }
    ],

    //función de inicio
    init: function(){

        var pois= WorldBase.pois;

        for (var i=0;i<pois.length;i++){
            var location=
                new AR.GeoLocation(
                pois[i].lat,
                pois[i].long,
                pois[i].alt
                );

            pois[i].loc=location;

            var camDrawable=

                new AR.Label(
                    "¿..?",
                    2.4,
                    {
                        style:{
                            backgroundColor:"#000000",
                            textColor:"#FF0000"
                        }
                    }
                );
                pois[i].label=camDrawable;

                var bombDrawable=new AR.ImageDrawable(new AR.ImageResource("bomb.png"),1.5);
                pois[i].bombDrawable=bombDrawable;

                var indicatorDrawable= new AR.ImageDrawable(
                    new AR.ImageResource("fireball.png"),
                    0.1
                );

                var geoObject = new AR.GeoObject(
                location,
                {
                    drawables: {
                        cam:[bombDrawable,pois[i].label],
                        indicator: indicatorDrawable
                    }
                }
                );
        }
    }

};

AR.context.onLocationChanged = function(lat,long,alt,acc){
    //alert("location changed");
    if(loaded==true){

        for(var i=0;i<WorldBase.pois.length;i++){

            var local = WorldBase.pois[i].loc;
            var distToBomb=Math.round(local.distanceToUser());
            WorldBase.pois[i].label.text=distToBomb;

            var actionRange=new AR.ActionRange(WorldBase.pois[i].loc,25,{
                onEnter:function(){
                    //alert("Estas a menos de 25 metros de una bomba");
                    info("Estás a menos de 25 metros de una bomba");
                },
                onExit:function(){
                    info("");
                }
            });

            if(distToBomb<50){
               WorldBase.pois[i].label.enabled=false;
               WorldBase.pois[i].bombDrawable.enabled=true;
            }else{
               WorldBase.pois[i].label.enabled=true;
               WorldBase.pois[i].bombDrawable.enabled=false;
            }
        }

    }else{
    loaded=true;
    WorldBase.init();
    }
}

function info(message){
    document.getElementById('info').innerHTML = message;
}
