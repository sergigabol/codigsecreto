
    var miUbicacion;

    var ubicacionObjetivo;
    var objetivo1Drawable;
    var objetivo2Drawable;
    var objetivo3Drawable;
    var objetivo4Drawable;
    var indicatorObjetivoDrawable;
    var geoObjetivo1;
    var geoObjetivo2;
    var geoObjetivo3;
    var geoObjetivo4;

    var ubicacionParca1;
    var ubicacionParca2;
    var ubicacionParca3;
    var ubicacionParca4;
    var parcaDrawable;
    var indicatorParcaDrawable;
    var geoParca1;
    var geoParca2;
    var geoParca3;
    var geoParca4;
    // Numero de Objetivos y Armas conseguidas.
    var objetivosConseguidos = 0;
    // Numero de Parcas en el juego.
    var parcaNumber = 0;

   // Nivel de Precision
   var accuracy = 5;

   // Distancia minima
   var minDistance = 30;

   // Ubicacion aleatoria;
   var latitud;
   var longitud;

var WorldMission2 = {

    init:function(lat,lon,alt){
        // Inicializamos MI ubicacion.
        miUbicacion = new AR.GeoLocation(lat,lon,alt);

        parcaDrawable = new AR.ImageDrawable(new AR.ImageResource("muerte.png"), 5);
        indicatorParcaDrawable = new AR.ImageDrawable (new AR.ImageResource("direccion_muerte.gif"),0.1,
                    { offsetY: -0.02, rotation: -120 });

        indicatorObjetivoDrawable = new AR.ImageDrawable (new AR.ImageResource("direccion_ubicacion.png"),0.1,
                    { offsetY: -0.02 });

        elegirUnaUbicacion(lat, lon, alt);

        createNewParca(latitud, -longitud, 0);
        createNewObjective(-latitud, longitud, 0);

        parcaNumber++;

        var radarPoint = new AR.Circle(0.05, {
            style:{ fillcolor: "#F7FE2E"}
        });

        AR.radar.container = document.getElementById("radarDiv");
        AR.radar.background = new AR.ImageResource("dragon-ball.png");
        //AR.radar.maxDistance= 100;
        //AR.radar.northIndicator.image = new AR.ImageResource("direccion_ubicacion.png");
        AR.radar.enabled = true;

    }
};

var load = false;
AR.context.onLocationChanged = function(lat,lon,alt,acc) {
    if(!load){
        //alert("latitud = " + lat + ", longitud = " + lon);
        // Inicializaremos el primer punto a encontrar y la primera parca.
        WorldMission2.init(lat,lon,alt);
        load = true;
    } else {
        if(ubicacionObjetivo.distanceToUser()<accuracy){
            alert("Enhorabuena! has alcanzado tu primera arma!!!");
            recogerObjetivoArma(lat, lon, alt);
        } else {
            // Actualizar tu ubicacion y la de la parca.
        }
        //actualizar
    }
};
// function createNewParca(){ definimos la funcion }
var createNewParca = function(lat, lon, alt){
    if (parcaNumber == 0){
        ubicacionParca1 = new AR.RelativeLocation(miUbicacion,lat,lon,alt);

        geoParca1 = new AR.GeoObject(
                    ubicacionParca1,
                    {
                    drawables:
                        {
                            cam:[parcaDrawable],
                            indicator: indicatorParcaDrawable
                        },
                    onClick:function(){
                            if(objetivosConseguidos>0){
                                // Eliminamos la parca!
                                geoParca1.destroy();
                                objetivosConseguidos--;
                                alert("La parca ha sido destruida!");
                            } else {
                                alert("No puedes eliminar la parca, busca un arma!!! Rapido!!!");
                            }
                        }
                    });

    } else if(parcaNumber == 1){
        ubicacionParca2 = new AR.RelativeLocation(miUbicacion, lat, lon, alt);
        geoParca2 = new AR.GeoObject(
                    ubicacionParca2,
                    {
                    drawables:
                        {
                            cam:[parcaDrawable],
                            indicator: indicatorParcaDrawable
                        },
                    onClick:function(){
                            if(objetivosConseguidos>0){
                                // Eliminamos la parca!
                                geoParca2.destroy();
                                objetivosConseguidos--;
                                alert("La parca ha sido destruida");
                            } else {
                                alert("No puedes eliminar la parca, busca un arma!!! Rapido!!!");
                            }

                        }
                    }
                );
    } else if (parcaNumber == 2){
        ubicacionParca3 = new AR.RelativeLocation(miUbicacion, lat, lon, alt);

        geoParca3 = new AR.GeoObject(
                    ubicacionParca3,
                    {
                    drawables:
                        {
                            cam:[parcaDrawable],
                            indicator: indicatorParcaDrawable
                        },
                    onClick:function(){
                            if(objetivosConseguidos>0){
                                // Eliminamos la parca!
                                geoParca3.destroy();
                                objetivosConseguidos--;
                                alert("La parca ha sido destruida");
                            } else {
                                alert("No puedes eliminar la parca, busca un arma!!! Rapido!!!");
                            }

                        }
                    }
                );
    } else if (parcaNumber == 3){
        ubicacionParca4 = new AR.RelativeLocation(miUbicacion, lat, lon, alt);

        geoParca4 = new AR.GeoObject(
                    ubicacionParca4,
                    {
                    drawables:
                        {
                            cam:[parcaDrawable],
                            indicator: indicatorParcaDrawable
                        },
                    onClick:function(){
                            if(objetivosConseguidos>0){
                                // Eliminamos la parca!
                                geoParca4.destroy();
                                objetivosConseguidos--;
                                alert("La parca ha sido destruida");
                            } else {
                                alert("No puedes eliminar la parca, busca un arma!!! Rapido!!!");
                            }

                        }
                    }
                );
    } else {
        //alert("Tienes suerte! no habran mas parcas! Mata las que queden");
    }
};

var createNewObjective = function(lat, lon, alt){
    ubicacionObjetivo = new AR.RelativeLocation(miUbicacion,lat,lon,0);

    if(parcaNumber == 0){
        objetivo1Drawable = new AR.ImageDrawable(new AR.ImageResource("weapon_beretta.png"), 5);
        geoObjetivo1 = new AR.GeoObject(
                                ubicacionObjetivo,
                                {
                                    drawables:
                                    {
                                        cam: [objetivo1Drawable],
                                        indicator: indicatorObjetivoDrawable
                                    },
                                    onClick:function()
                                    {
                                        geoObjetivo1.destroy();
                                        recogerObjetivoArma(lat, lon, alt);
                                        alert("Has recogido un Arma! Ahora pelea!!!");
                                    }
                                }
                            );
    } else if(parcaNumber == 1){
        var objetivo2Drawable = new AR.ImageDrawable(new AR.ImageResource("weapon_g3.png"), 5);

        geoObjetivo2 = new AR.GeoObject(
                    ubicacionObjetivo,
                    {
                        drawables:
                        {
                            cam: [objetivo2Drawable],
                            indicator: indicatorObjetivoDrawable
                        },
                        onClick:function(){
                            //Recogemos el arma
                            geoObjetivo2.destroy();
                            recogerObjetivoArma(lat, lon, alt);
                            alert("Has recogido un Arma! Ahora pelea!!!");
                        }
                    }
                );

    } else if (parcaNumber == 2){
        var objetivo3Drawable = new AR.ImageDrawable(new AR.ImageResource("weapon_m416.png"), 5);

        geoObjetivo3 = new AR.GeoObject(
                    ubicacionObjetivo,
                    {
                        drawables:
                        {
                            cam: [objetivo3Drawable],
                            indicator: indicatorObjetivoDrawable
                        },
                        onClick:function(){
                            //Recogemos el arma
                            geoObjetivo3.destroy();
                            recogerObjetivoArma(lat, lon, alt);
                            alert("Has recogido un Arma! Ahora pelea!!!");
                        }
                    }
                );

    } else if (parcaNumber == 3){
        var objetivo4Drawable = new AR.ImageDrawable(new AR.ImageResource("weapon_rifledeasalto.png"), 5);

        geoObjetivo4 = new AR.GeoObject(
                    ubicacionObjetivo,
                    {
                        drawables:
                        {
                            cam: [objetivo4Drawable],
                            indicator: indicatorObjetivoDrawable
                        },
                        onClick:function(){
                            //Recogemos el arma
                            geoObjetivo4.destroy();
                            recogerObjetivoArma(lat, lon, alt);
                            alert("Has recogido un Arma! Ahora pelea!!!");
                        }
                    }
                );
    } else {
        alert("Ya no quedan mas armas que conseguir! Mata todas las parcas que queden para acabar!");
        // FINALIZAR JAVASCRIPT (ArchitectView)
    }
};

var recogerObjetivoArma = function(lat, lon, alt){
    objetivosConseguidos++;
    miUbicacion = new AR.GeoLocation(lat,lon,alt);
    elegirUnaUbicacion(lat, lon, alt);
    createNewParca(latitud, -longitud, 0);
    createNewObjective(-latitud, longitud, 0);
    parcaNumber++;
}

var elegirUnaUbicacion = function(lat, lon, alt){
    var entrar = false;
    var ubicacionficticia;
    while(!entrar){
        latitud = Math.floor((Math.random() * 100) + 1);
        longitud = Math.floor((Math.random() * 100) + 1);
        ubicacionficticia = new AR.RelativeLocation(miUbicacion,latitud,longitud,alt);
        if (ubicacionficticia.distanceToUser()>=minDistance){
            entrar = true;
        }
    }
    alert("latitud = " + latitud + " y longitud = " + longitud);
    alert("Estas a " + ubicacionficticia.distanceToUser() + " metros de tu objetivo");
}