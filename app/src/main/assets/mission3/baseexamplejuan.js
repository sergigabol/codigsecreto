var World = {

    pois : [
        {
            lat: 41.416914,
            lon: 2.133067,
            alt: 260,
            desc:"aaa"
        },{
            lat: 41.417195,
            lon: 2.132855,
            alt: 265,
            desc:"bbb"
        }

    ],

    init:function(){

        var pois = World.pois;
        for(var i=0; i<pois.length; i++) {
            var location = new AR.GeoLocation(
                               pois[i].lat,
                               pois[i].lon,
                               pois[i].alt
            );

            var nearLocation = new AR.RelativeLocation(
                                   location,
                                   10,
                                   -15,
                                   0
            );

            var camDrawable = new AR.Label(
                                  pois[i].desc,
                                  5,
                                  {
                                    style: {
                                        backgroundcolor: "#FF0000",
                                        textColor: "#00FF00"
                                    }
                                  }
            );

            var indicatorDrawable = new AR.ImageDrawable (
                new AR.ImageResource("assets/indi.png"),
                0.1
            );

            var geoObject = new AR.GeoObject(
                location,
                {
                    drawables : {
                        cam: [camDrawable],
                        indicator: indicatorDrawable
                    }
                }

            );

            var relativeGeoObject = new AR.GeoObject(
                nearLocation,
                {
                    drawables : {
                        cam: [camDrawable]
                    }
                }
            );

        }
    }
};

var load = false;
AR.context.onLocationChanged = function (lat,lon,alt,acc) {

    if(!load){
        alert("localizacion inicializada");
        World.init();
        load = true;
    } else {
        //actualizar
    }


};