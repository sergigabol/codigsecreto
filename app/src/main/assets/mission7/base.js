var estemSalvats = false;
var contador = 0;
var bombaActiva = null;
var nivellTerra = 0;

var WorldBase={

    init: function(){

        //alert("init");

        // Definim els objectes que es mostrar�n per pantalla: Indicador, Bomba (3D) i Explosi�.
        this.bombIndicator=new AR.ImageDrawable(new AR.ImageResource("indi.png"),0.1);

        this.bombDrawable=new AR.ImageDrawable(new AR.ImageResource("bomb_web.png"),5,{
            rotation:45,
            offsetY: 10});

		this.bombExplosion = new AR.AnimatedImageDrawable(new AR.ImageResource("explosion.png"), 10, 128, 128,{enabled:true});

		this.bombFire = new AR.AnimatedImageDrawable( new AR.ImageResource("fire.png"),5,200,200,{enabled:true});

        //I apa... a tirar bombes
        this.throwBomb();
    },

    targetLocation: null,

    targetLocked: false,

    throwBomb: function(){

        info("INICIA BOMBA");
       // Inicialitza la posici� inicial de la bomba
        if (WorldBase.targetLocked){

            this.bombLocation = WorldBase.targetLocation;

            //this.bombLocation = new AR.RelativeLocation(null,4,0,0);


            this.bombGeoObject = new AR.GeoObject(this.bombLocation,{
                               drawables: {
                                           cam: [this.bombDrawable],
                                           indicator: [this.bombIndicator]
                                          }
            });

            var actionRange = new AR.ActionRange(this.bombLocation, 20,{
                          onExit:function(){
                              actionRange.enabled=false;
                              estemSalvats = true;
                              // Posar avis de que ja estas a salvo.
                          }
            });

            //dexem caure la bomba
            estemSalvats = false;
            bombaActiva = setInterval(bombStep,100);
        }
    },


};

AR.context.onLocationChanged = function(lat,long,alt,acc){
    if (!WorldBase.targetLocked){
        nivellTerra = alt;
        WorldBase.targetLocation = new AR.GeoLocation(lat,long,100.0);
        WorldBase.targetLocked = true;
        WorldBase.throwBomb();
    }
    contador = contador + 1;
    info("lat:"+lat+", long:"+long + ", cont:"+contador);

    // Indicar a quants metres estem del punt d'explosi�.
}

/* Funció per anar printant missatges en la part html */
function info(message){
    document.getElementById('info').innerHTML = message;
};

//***********************CONTROLA LA CAIGUDA I EXPLOSIO DE LA BOMBA ******************************
 function bombStep(){
        // Redu�m la altitud de la bomba 1m 1/10s
        WorldBase.bombDrawable.offsetY -= 0.1;

        info("Altitud bomba "+WorldBase.bombDrawable.offsetY+" m.");

        // Controlem si ha explotat la bomba
        if (WorldBase.bombDrawable.offsetY <= -1.7){
            // Desactivem la caiguda
            clearInterval(bombaActiva);
            // Fem explosi�
            WorldBase.bombGeoObject.drawables.removeCamDrawable(WorldBase.bombDrawable);
            //WorldBase.bombGeoObject.drawables.removeIndicatorDrawable(WorldBase.bombIndicator);
            WorldBase.bombGeoObject.drawables.addCamDrawable(WorldBase.bombExplosion);
		    WorldBase.bombExplosion.animate([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 100, 1);
            WorldBase.bombGeoObject.drawables.addCamDrawable(WorldBase.bombFire);
		    WorldBase.bombFire.animate([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 100, -1);
            //Llançem la animació de la booomba. (sprite)
            if (estemSalvats){
                info("Te has salvado!!!");

            }else{
                info("ESTAS MUERTOOO!!");
            }
        }
    }

WorldBase.init();
