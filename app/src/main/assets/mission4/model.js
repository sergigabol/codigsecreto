var Model = {

    geoObjectArray: [],

    geoLocationArray: [],

    puntoActivo: 0,

    pois : [
        {
            lat: 41.417039,
            lon: 2.132674,
            alt: 260,
            desc:"POI 1r"
        },{
            lat: 41.417004,
            lon: 2.132672,
            alt: 265,
            desc:"POI 2n"
        }

    ],

     calienteHtml : "<p style='color:red;'>Caliente !!</p>",
     frioHtml : "<p style='color:blue;'>Frioo !!</p>",
     puntoHtml: "<div><h2 style='color:white;'>Enhorabuena !!</h2><p style='color:white;'>Alcanzado objetivo</p><p style='color:white;'>Click para guardar!</p></div>"

};