var World = {
/*
    geoObjectArray: [],

    geoLocationArray: [],

    puntoActivo: 0,

    pois : [
        {
            lat: 41.417039,
            lon: 2.132674,
            alt: 260,
            desc:"POI 1r"
        },{
            lat: 41.417004,
            lon: 2.132672,
            alt: 265,
            desc:"POI 2n"
        }

    ],*/

    init:function(){

        var pois = Model.pois;

        for(var i=0; i<pois.length; i++) {

            // Puntos
            this.geoLocationArray[i] = new AR.GeoLocation(
                               pois[i].lat,
                               pois[i].lon,
                               pois[i].alt
            );

/*
            var nearLocation = new AR.RelativeLocation(
                                   this.geoLocationArray[i],
                                   10,
                                   -15,
                                   0
            );
*/

/*
            // Label
            var camDrawable = new AR.Label(
                                  pois[i].desc,
                                  5,
                                  {
                                    style: {
                                        backgroundcolor: "#FF0000",
                                        textColor: "#00FF00"
                                    }
                                  }
            );

*/


//
//            // Indicador de direccion
//            var indicatorDrawable = new AR.ImageDrawable (
//                new AR.ImageResource("assets/indi.png"),
//                0.1
//            );

            // Radar
            var radarCircle = new AR.Circle(0.03, {
                                horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
                                opacity: 0.8,
                                style: {
                                    fillColor: "#ff0000"
                                }
            });

             var radardrawables = [];
             radardrawables.push(radarCircle);

             var indicatorRadar = new AR.ImageDrawable (
                            new AR.ImageResource("assets/ribbon_1.png"),
                            0.5
             );

/*
            // GeoObject
            this.geoObjectArray[i] = new AR.GeoObject(
                this.geoLocationArray[i], // Location
                {
                    enabled : true,
                    drawables : {
                        cam: [camDrawable],
                       // indicator: indicatorDrawable,
                        radar: [indicatorRadar]
                    }
                }

            );
*/

/*
            var relativeGeoObject = new AR.GeoObject(
                nearLocation,
                {
                    drawables : {
                        cam: [camDrawable]
                    }
                }
            );
*/

        }

    }

};

var loaded = false;
var distanciaAnterior = 0; // Ultima distancia conocida al punto a buscar

var visible = false; // estado del geoObject

AR.context.onLocationChanged = function (lat,lon,alt,acc) {
    var punto = World.geoLocationArray[World.puntoActivo];
    var objeto = World.geoObjectArray[World.puntoActivo];

    if (!loaded) {
        World.init();
        loaded = true;
        PoiRadar.show();
        info("Busca el primer punto");
    } else {

        // Mostrar mensaje
        info(getMessageFrom(punto));

        // Comprobar si hemos entrado en su radio para mostrarlo
        // Agregar el drawable al geoObject ??? y que no tenga previamente

        // if(visible) ...

    }

};



// Obtiene el mensaje correspondiente, dependiendo de si te estas acercando o alejando.
function getMessageFrom(point) {
    var caliente = "<p style='color:red;'>Caliente !!</p>";
    var frio = "<p style='color:blue;'>Frioo !!</p>";
    var mensaje = caliente;
    var metros = point.distanceToUser();
    if(metros > distanciaAnterior){
         mensaje = frio;

    }
    distanciaAnterior = metros;
    return mensaje;
}


// Muestra un mensaje durante 3 segundos.
function info(message){
    document.getElementById('info').innerHTML = message;
    setTimeout(function(){
        document.getElementById('info').innerHTML = "";
    },3000)
}