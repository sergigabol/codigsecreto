var World = {



    init: function(){

        var pois = Model.pois;
        this.distanciaAnterior = 0;

        for(var i=0; i<pois.length; i++) {

            // Puntos
            Model.geoLocationArray[i] = new AR.GeoLocation(
                               pois[i].lat,
                               pois[i].lon,
                               pois[i].alt
            );


            // Radar
            var radarCircle = new AR.Circle(0.03, {
                                horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
                                opacity: 0.8,
                                style: {
                                    fillColor: "#ff0000"
                                }
            });

             var radardrawables = [];
             radardrawables.push(radarCircle);

             var indicatorRadar = new AR.ImageDrawable (
                            new AR.ImageResource("assets/ribbon_1.png"),
                            0.5
             );


            // GeoObject
            Model.geoObjectArray[i] = new AR.GeoObject(
                Model.geoLocationArray[i], // Location
                {
                    enabled : true,
                    drawables : {
                       //cam: [camDrawable],
                       // indicator: indicatorDrawable,
                        radar: [indicatorRadar]
                    }
                }

            );

        }

        this.punto = Model.geoLocationArray[Model.puntoActivo];
        this.objeto = Model.geoObjectArray[Model.puntoActivo];

        this.setActionRange(this.punto);
        this.showInfo("Busca el primer punto");

    },

    // Activa un ActionRange para un GeoLocation
    setActionRange: function(location) {

        var self = this;
        this.actionRange = new AR.ActionRange(location, 150,{
            onEnter: function() {
                alert("Entra en rango");
                self.setNextPoint();
            }

        });
    },

    showHtml: function(){
         alert("Entra en showHtml");
        var self = this;

        // Bandera que se muestra al pasar al siguiente punto
        var banderaDrawable = new AR.ImageDrawable(new AR.ImageResource("assets/red-flag.png"),1);

        //create a new html drawable and pass some setup parameters to it
        var htmlDrawable = new AR.HtmlDrawable({html:Model.puntoHtml}, 1, {
          offsetX : 1,
          onClick : function() {

            // Cambiamos el drawable
            self.objeto.drawables.cam = [banderaDrawable];

            // Cambiamos el punto y objeto activos
            Model.puntoActivo += 1;
            self.punto = Model.geoLocationArray[Model.puntoActivo];
            self.objeto = Model.geoObjectArray[Model.puntoActivo];


            self.showInfo("Busca el siguiente punto !!");


          },
          horizontalAnchor : AR.CONST.HORIZONTAL_ANCHOR.LEFT,
          opacity : 0.9,
          backgroundColor: "#0000FF"
        });

        this.objeto.drawables.cam = [htmlDrawable];
    },

    setNextPoint: function() {
         alert("Entra en setNextPoint");
        this.showHtml();

    },


    // Si nos alejamos es false
    distanceHandler: function(point) {

        var result = true;
        var metros = point.distanceToUser();
        if(metros > this.distanciaAnterior){
            result = false;
        }
        this.distanciaAnterior = metros;

        return result;
    },

    showInfo: function(message) {
         document.getElementById('info').innerHTML = message;
         setTimeout(function(){
             document.getElementById('info').innerHTML = "";
         },3000)
    },

    locationHandler: function(punto){
        if(this.distanceHandler(punto)){
            this.showInfo(Model.calienteHtml);
        }else{
            this.showInfo(Model.frioHtml);
        }



    },

    isInRange: function() {
        var point = Model.puntoActivo;


    }



};

var loaded = false;


AR.context.onLocationChanged = function (lat,lon,alt,acc) {


    if (!loaded) {
        World.init();
        loaded = true;
        PoiRadar.show();

//                var punto = Model.geoLocationArray[Model.puntoActivo];
//                var objeto = Model.geoObjectArray[Model.puntoActivo];
//                World.setActionRange(punto);
//        World.showInfo("Busca el primer punto");


    } else {


            var punto = Model.geoLocationArray[Model.puntoActivo];
             var objeto = Model.geoObjectArray[Model.puntoActivo];
        World.locationHandler(punto);

    }

};


