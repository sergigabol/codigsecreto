var WorldBase={

    init: function(){

        //alert("init");

        this.indicatorDrawable=new AR.ImageDrawable(new AR.ImageResource("indi.png"),0.1);

        //set up the home

        this.deathLocation=new AR.RelativeLocation(null, 20,20,2);

        //set up each of the N locations with the code digits

        var deathDrawable=new AR.ImageDrawable(new AR.ImageResource("death.png"),3);

        this.createMarker({"desc":"Home"}, this.deathLocation,deathDrawable);

    },

    createMarker: function(poiData, poiLocation, imageDrawable,createRange){

        /*
        var poiLabel = new AR.HtmlDrawable({html:"<div>POI "+poiData.desc+"</div>"}, 1, {
                  offsetX : 1,
                  onClick : function() {
                    htmlDrawable.html += "<div>Another div</div>";
                  },
                  horizontalAnchor : AR.CONST.HORIZONTAL_ANCHOR.LEFT,
                  opacity : 0.9
                });
        */
        var poiLabel;
        if(imageDrawable){
            poiLabel=imageDrawable;
        }else{
            poiLabel=new AR.Label("POI "+poiData.desc, 1, {
                    opacity : 0.6,
                    style:{
                        backgroundColor:"#FF0000",
                        textColor:"#00FF00"
                    }
                });
        }


        var radarPoint=new AR.Circle(0.05,{
            style:{
                fillColor: "#F7FE2E"
            }
        });

        var markerObject = new AR.GeoObject(poiLocation, {
                drawables: {
                    cam: poiLabel,
                    indicator: [WorldBase.indicatorDrawable],
                    radar: radarPoint
                }

            });
        //alert("created location");


        if(createRange){
            var actionRange = new AR.ActionRange(poiLocation, 20,{
                onEnter:function(){
                    actionRange.enabled=false;
                    actionRange.markerObject.drawables.addCamDrawable(WorldBase.poiVisitedDrawable);
                    actionRange.markerObject.drawables.removeCamDrawable(WorldBase.poiDrawable);
                    WorldBase.poisToVisit--;
                    if(WorldBase.poisToVisit==0){
                        alert("Great! You saved yourself!");
                        clearInterval(WorldBase.deathStep);
                    }
                }
            });
            actionRange.markerObject=markerObject;

        }

        return markerObject;
    },

    poisInitialized:false,

    deathHalfWay:false,
    deathNear:false,

    deathStep: function(){
        WorldBase.deathLocation.easting-=0.25;
        WorldBase.deathLocation.northing-=0.25;
        info("You have to visit "+WorldBase.poisToVisit+" places to save your soul, and the death is at "
            +WorldBase.deathLocation.easting+"m");
        if(WorldBase.deathLocation.easting<10 && !WorldBase.deathHalfWay){
            WorldBase.deathHalfWay=true;
            info("The death is the half way: "+WorldBase.deathLocation.easting+"m");
        }else if(WorldBase.deathLocation.easting<5 && !WorldBase.deathNear){
            WorldBase.deathNear=true;
            info("The death is near!!! "+WorldBase.deathLocation.easting+"m");
        }else if(WorldBase.deathLocation.easting<1){
            alert("Game Over, the death cached you");
            clearInterval(WorldBase.deathStep);
        }
    },

    initRadar:function (){
        AR.radar.container=
            document.getElementById("radarDiv");

        AR.radar.background=new AR.ImageResource("dbradar.png");
        //AR.radar.maxDistance=100;

        AR.radar.northIndicator.image=new AR.ImageResource("indi.png");

        AR.radar.enabled=true;
    },

    poisToVisit:0,
    poiVisitedDrawable:new AR.ImageDrawable(new AR.ImageResource("visited.png"),2)

};
WorldBase.init();
AR.context.onLocationChanged = function(lat,long,alt,acc){
    if(!WorldBase.poisInitialized){
        //alert("location changed, updating pois");
        WorldBase.poiDrawable=new AR.ImageDrawable(new AR.ImageResource("not-visited.png"),2);
        for(var i=0;i<pois.length; i++){
            WorldBase.createMarker(
                pois[i],
                new AR.GeoLocation(pois[i].latitude, pois[i].longitude, pois[i].altitude),
                WorldBase.poiDrawable,
                true
            );
        }
        WorldBase.poisToVisit=pois.length;
        info("You have to visit "+WorldBase.poisToVisit+" places to save your soul");
        WorldBase.poisInitialized=true;

        WorldBase.initRadar();

    }else{

    }
};
setInterval(WorldBase.deathStep,5000);
function info(message){
    document.getElementById('info').innerHTML = message;
}