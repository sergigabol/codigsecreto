//juan manuel y Salah.

//Crea 5 localizaciones a partir de la inicial del usuario.
//Estas localizaciones disponen de un indicador de dirección para ayudar a encontrarlas.
//Cuando el jugador está a 25 o menos metros de una localización se muestra su imagen (un color),
// antes son invisibles.
//Si se encuentra a 5 o menos se considera conseguido y su representación cambia por una estilo
//checked.

//Cuando se consiguen todos los objetivos se han de ordenar de determinada manera elegida aleatoriamente.

var localizado = false;
var posUser;
var objetivos = 4;
World ={
	init:function(){
		var longitud = posUser.longitude;
		var latitud = posUser.latitude;
		var imgAmarillo = new AR.ImageResource("amarillo.jpg");
		var imgAzul = new AR.ImageResource("azul.jpg");
		var imgNaranja = new AR.ImageResource("naranja.jpg");
		var imgVerde = new AR.ImageResource("verde.jpg");
		var imgConseguido = new AR.ImageResource("conseguido.png");

		var dwbAmarillo = new AR.ImageDrawable(imgAmarillo, 1);
		var dwbAzul = new AR.ImageDrawable(imgAzul, 1);
		var dwbNaranja = new AR.ImageDrawable(imgNaranja, 1);
		var dwbVerde = new AR.ImageDrawable(imgVerde, 1);
		var dwbConseguido = new AR.ImageDrawable(imgConseguido, 1);

		this.indicatorDrawable=new AR.ImageDrawable(new AR.ImageResource("indi.png"),0.1);

		//Las posiciones relativas para los objetivos.
		posAmarillo = this.creaRelativeLocation(posUser);
		posAzul = this.creaRelativeLocation(posUser);
		posNaranja = this.creaRelativeLocation(posUser);
		posVerde = this.creaRelativeLocation(posUser);

		//TODO:Las marcas para mostrar los colores encontrados se han de crear de forma dinámica
		//TODO:y han de mantenerse en la dirección en la que mira el jugador....
		//Los relative location para la UI
//		var amarillo = new AR.RelativeLocation(posUser, poisUser[0].norte, poisUser[0].este);
//		var azul = new AR.RelativeLocation(posUser, poisUser[1].norte, poisUser[1].este);
//		var naranja = new AR.RelativeLocation(posUser, poisUser[2].norte, poisUser[2].este);
//		var verde = new AR.RelativeLocation(posUser, poisUser[3].norte, poisUser[3].este);
//		marAmarillo = this.createMarker(poisUser[0].id, amarillo, dwbAmarillo);
//		marAzul = this.createMarker(poisUser[1].id, azul, dwbAzul);
//		marNaranja = this.createMarker(poisUser[2].id, naranja, dwbNaranja);
//		marVerde = this.createMarker(poisUser[3].id, verde, dwbVerde);

		//Crea las marcas de los objetivos.
		marPosAmarillo = this.createMarker(pois[0].id, posAmarillo);
		marPosAzul = this.createMarker(pois[1].id, posAzul);
		marPosNaranja = this.createMarker(pois[2].id, posNaranja);
		marPosVerde = this.createMarker(pois[3].id, posVerde);

		//Se crean las áreas de acción de los objetivos.
		arAmarillo = this.creaAreaAccion(posAmarillo, marPosAmarillo, dwbAmarillo, false);
		arAzul = this.creaAreaAccion(posAzul, marPosAzul, dwbAzul, false);
		arNaranja = this.creaAreaAccion(posNaranja, marPosNaranja, dwbNaranja, false);
		arVerde = this.creaAreaAccion(posVerde, marPosVerde, dwbVerde, false);
	},

    createMarker: function(poiId, poiLocation, imageDrawable, addImg){
    	var poiId = poiId;
    	var norte = poiLocation.northing;
    	var este = poiLocation.easting;
    	var uimarca = imageDrawable;
    	var encontrado = false;


    	var overlayMarker = new AR.GeoObject(poiLocation,
    		{
    			drawables:{
//    				cam:uimarca,
    				indicator:[World.indicatorDrawable]
    			}
    		}
    	);
    	if(addImg){
    		overlayMarker.addCamDrawable(uimarca);
    	}
    	return overlayMarker;
    },

    creaRelativeLocation: function(geoReferencia){
    	var palNorte = Math.floor((Math.random() * 50) + 1);
    	var palEste = Math.floor((Math.random() * 50) + 1);
		var posONeg = Math.random();
		var nortOEste = Math.random();
		if(posONeg < 0.5){
			if(nortOEste >= 0.5){
				palNorte *= -1;
			}else{
				palEste *= -1;
			}
		};
    	return new AR.RelativeLocation(geoReferencia, palNorte, palEste);
    },

    creaAreaAccion(localizacion, marca){
    	var marcaObj = marca;
    	var areaAccion = new AR.ActionRange(localizacion, 25,{
    		onEnter:function(){
    			if(!marcaObj.encontrado){
    				areaAccion.radius = 5;
    				marcaObj.encontrado = true;
					marcaObj.addCamDrawable(marcaObj.uimarca);
    				//TODO:Crea o activa el indicador en la ui de color encontrado...
    			}else{
    				objetivos--;
    				marcaObj.uimarca = dwbConseguido;
					areaAccion.enabled = false;
    			}
    		}
    	});
    	return areaAccion;
    }
};//End World

AR.context.onLocationChanged = function(lat,long,alt,acc){
	if(!localizado){
		localizado = true;
		posUser =  new AR.GeoLocation(lat, long);
		var locLatitud = posUser.latitude;
		var locLongitud = posUser.longitude;
		alert("Tu longitud = "+locLongitud+", tu latitud = "+locLatitud);
		World.init();
	}
};